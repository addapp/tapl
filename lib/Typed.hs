{-# LANGUAGE ViewPatterns #-}

module Typed where

data Term =
  TmTrue
  | TmFalse
  | TmIf Term Term Term
  | TmVar Int Int -- second it is total length of context in which variable appears
  | TmAbs String Ty Term -- string is hint for var name when printing
  | TmApp Term Term
  deriving (Show)

type ParsedTerm = Maybe Term

-- instance Show Term where
  -- show

data Ty = TyBool | TyArr Ty Ty deriving (Show, Eq)
data Binding = NameBind | VarBind Ty deriving (Show)
type Context = [(String, Binding)]

printtm :: Context -> Term -> Either String String
printtm ctx (TmAbs s ty t1) = do
  let (ctx', s') = pickfreshname ctx s
  st1 <- printtm ctx' t1
  return $ "(\\ " ++ s' ++ ":" ++ show ty ++ ". " ++ st1 ++ ")"
printtm ctx (TmApp t1 t2) = do
  st1 <- printtm ctx t1
  st2 <- printtm ctx t2
  return $ "(" ++ st1 ++ " " ++ st2 ++ ")"
printtm ctx (TmVar x n) =
  if length ctx == n
  then Right $ indexToName ctx x
  else Left $ "Bad index: " ++ show n

indexToName :: Context -> Int -> String
indexToName ctx i = fst $ ctx !! i

inContext :: String -> Context -> Bool
inContext s ctx = s `elem` (fst <$> ctx)

pickfreshname :: Context -> String -> (Context, String)
pickfreshname ctx s = if test s ctx then ((s, NameBind) : ctx, s)
                      else helper s 1 ctx
  where test s ctx = not $ s `inContext` ctx
        helper s n ctx = let s' = s ++ show n
                         in if test s' ctx
                            then ((s', NameBind) : ctx, s')
                            else helper s (n+1) ctx

addBinding :: Context -> String -> Binding -> Context
addBinding ctx x bind = (x,bind) : ctx

getBinding ctx i = snd $ ctx !! i

getTypeFromContext :: Context -> Int -> Either String Ty
getTypeFromContext ctx i = case getBinding ctx i of
                             VarBind ty -> Right ty
                             _ -> Left $ "Expected Varbind at " ++ show i

-- |The typeof function just implements the type inference rules or rather the inversion lemmas by asserting that some types have a certain form depending on the term at hand.
typeof :: Context -> Term -> Either String Ty
typeof ctx TmTrue = Right TyBool
typeof ctx TmFalse = Right TyBool
typeof ctx (TmIf t1 t2 t3) = do
  tt1 <- typeof ctx t1
  if tt1 == TyBool then do
    tt2 <- typeof ctx t2
    tt3 <- typeof ctx t3
    if tt2 == tt3 then Right tt2
      else Left "Types of branches don't match"
    else Left "Condition must have type bool"
typeof ctx (TmVar i _) = getTypeFromContext ctx i
typeof ctx (TmAbs x ty t) = do
  let ctx' = addBinding ctx x (VarBind ty)
  tt <- typeof ctx' t
  return $ TyArr ty tt
typeof ctx (TmApp t1 t2) = do
  tt1 <- typeof ctx t1
  tt2 <- typeof ctx t2
  case tt1 of
    TyArr tt11 tt12 -> if tt11 == tt2 then Right tt12
                       else Left "Wrong parameter type"
    _ -> Left "Function type expected"

foldTerm :: (Int -> Int -> Int -> Term) -> Term -> Term
foldTerm f t = walk 0 t
  where walk c (TmVar x n) = f c x n
        walk c (TmApp t1 t2) = TmApp (walk c t1) (walk c t2)
        walk c (TmAbs s ty t1) = TmAbs s ty (walk (c+1) t1)
        walk c TmTrue = TmTrue
        walk c TmFalse = TmFalse
        walk c (TmIf t1 t2 t3) = TmIf (walk c t1) (walk c t2) (walk c t3)
  
termShift :: Int -> Term -> Term
termShift d t = foldTerm f t
  where f c x n = if x >= c then TmVar (x+d) (n+d)
                  else TmVar x (n+d)

termSubst :: Int -> Term -> Term -> Term
termSubst j s t = foldTerm f t
  where f c x n = if x == j + c then termShift c s
                  else TmVar x n

-- beta reduction first shifts the s up by one, then substitutes the first variable and then shifts down by one since the first variable was "used up"
termSubstTop s t = termShift (-1) (termSubst 0 (termShift 1 s) t)        

isval :: Term -> Bool
isval (TmAbs _ _ _) = True
isval TmTrue = True
isval TmFalse = True
isval _ = False

eval1 :: Context -> Term -> ParsedTerm
eval1 ctx (TmIf TmTrue t _) = Just t
eval1 ctx (TmIf TmFalse _ t) = Just t
eval1 ctx (TmApp (TmAbs _ _ t12) v2)
  | isval v2 = Just $ termSubstTop v2 t12
eval1 ctx (TmApp v1 t2)
  | isval v1 = do t2' <- eval1 ctx t2
                  return $ TmApp v1 t2'
eval1 ctx (TmApp t1 t2) = do t1' <- eval1 ctx t1
                             return $ TmApp t1' t2
-- to match all the rest, stuck terms and values we just return Nothing to cause evalSmall to print the final result.                             
eval1 _ _ = Nothing                             

evalSmall :: Context -> Term -> Term
evalSmall ctx t = let pt = eval1 ctx t
                  in case pt of
                       Nothing -> t
                       Just t' -> evalSmall ctx t'

term1 = TmApp
        (TmAbs "x" (TyArr TyBool TyBool)
          (TmApp (TmVar 0 1) TmFalse))
        (TmAbs "b" TyBool
          (TmIf (TmVar 0 1) TmFalse TmTrue))
