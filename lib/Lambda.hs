module Lambda where

-- Ex. 5.2.6
-- should be quadratic
-- prd will apply ss m times to zz
-- ss will plus-1 the second element of the pair which is linear in that element
-- so a linear operation called inside a linear loop should become quadratic

-- Ex 5.2.7
-- equal = \m. \n. and (iszro (sub m n)) (iszro (sub n m))

-- Ex 5.2.8
-- nil = \c. \n. n
-- cons = \h. \t. \c. \n. c h (t c n)
-- isnil = \t. t (\a. \b. fls) tru
-- head nil is nil
-- head = \t. t (\a. \b. a) nil
-- for tail we could write a cc that gets the next element and a pair (tail, head) and returns (head:tail, element) and then just take the first element of the pair
-- nn = pair nil nil
-- cc = \e. \p. pair (cons (snd p) (fst p)) e
-- tail = \t. fst (t cc nn)

-- Ex 5.2.9
-- Because it's easier to read I think.
-- g = \fct. \n. test (equal n c0) c1 (times n (fct (prd n)))

-- Ex. 5.2.10
-- g = \chnt. \n. if n=0 then (\s. \z. z) else \s. \z. (chnt n-1) s z
-- churchnat = fix g

-- Ex 5.2.11
-- It's much easier without fix
-- sum1 = \t. t plus 0
-- With fix we would essentially need to have a recursive function that always adds the head to the recursive call
-- g = \sm. \t. if isnil then 0 else plus (head t) (sm (tail t))
-- sum2 = fix g


main :: IO ()
main = putStrLn "Helo"
