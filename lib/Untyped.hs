{-# LANGUAGE ViewPatterns #-}

module Untyped where

data Term =
  TmVar Int Int -- second it is total length of context in which variable appears
  | TmAbs String Term -- string is hint for var name when printing
  | TmApp Term Term

type ParsedTerm = Maybe Term

-- instance Show Term where
  -- show

data Binding = NameBind deriving (Show)
type Context = [(String, Binding)]

printtm :: Context -> Term -> Either String String
printtm ctx (TmAbs s t1) = do
  let (ctx', s') = pickfreshname ctx s
  st1 <- printtm ctx' t1
  return $ "(\\ " ++ s' ++ ". " ++ st1 ++ ")"
printtm ctx (TmApp t1 t2) = do
  st1 <- printtm ctx t1
  st2 <- printtm ctx t2
  return $ "(" ++ st1 ++ " " ++ st2 ++ ")"
printtm ctx (TmVar x n) =
  if length ctx == n
  then Right $ indexToName ctx x
  else Left $ "Bad index: " ++ show n

indexToName :: Context -> Int -> String
indexToName ctx x = fst $ ctx !! x

inContext :: String -> Context -> Bool
inContext s ctx = s `elem` (fst <$> ctx)

pickfreshname :: Context -> String -> (Context, String)
pickfreshname ctx s = if test s ctx then ((s, NameBind) : ctx, s)
                      else helper s 1 ctx
  where test s ctx = not $ s `inContext` ctx
        helper s n ctx = let s' = s ++ show n
                         in if test s' ctx
                            then ((s', NameBind) : ctx, s')
                            else helper s (n+1) ctx

termShift :: Int -> Term -> Term
termShift d t = walk 0 t
  where walk c (TmVar x n) = if x >= c then TmVar (x+d) (n+d)
                             else TmVar x (n+d)
        walk c (TmApp t1 t2) = TmApp (walk c t1) (walk c t2)
        walk c (TmAbs s t1) = TmAbs s (walk (c+1) t1)

termSubst :: Int -> Term -> Term -> Term
termSubst j s t = walk 0 t
  where walk c t@(TmVar x n) = if x == j + c then termShift c s
                             else t
        walk c (TmApp t1 t2) = TmApp (walk c t1) (walk c t2)
        walk c (TmAbs s t1) = TmAbs s (walk (c+1) t1)

-- beta reduction first shifts the s up by one, then substitutes the first variable and then shifts down by one since the first variable was "used up"
termSubstTop s t = termShift (-1) (termSubst 0 (termShift 1 s) t)        

isval :: Term -> Bool
isval (TmAbs _ _) = True
isval _ = False

eval1 :: Context -> Term -> ParsedTerm
eval1 ctx (TmApp (TmAbs x t12) v2)
  | isval v2 = Just $ termSubstTop v2 t12
eval1 ctx (TmApp v1 t2)
  | isval v1 = do t2' <- eval1 ctx t2
                  return $ TmApp v1 t2'
eval1 ctx (TmApp t1 t2) = do t1' <- eval1 ctx t1
                             return $ TmApp t1' t2
eval1 _ _ = Nothing                             

evalSmall :: Context -> Term -> Term
evalSmall ctx t = let pt = eval1 ctx t
                  in case pt of
                       Nothing -> t
                       Just t' -> evalSmall ctx t'

-- Ex 7.3.1
eval2 :: Context -> Term -> ParsedTerm
eval2 ctx v
  | isval v = Just v
eval2 ctx (TmApp t1 t2)
  | Just (TmAbs _ t12) <- eval2 ctx t1
  , Just v2 <- eval2 ctx t2
  , isval v2
  , Just v <- eval2 ctx (termSubstTop v2 t12)
  , isval v = Just v
eval2 _ _ = Nothing

evalBig :: Context -> Term -> Term
evalBig ctx (eval2 ctx -> Just t) = t
evalBig ctx t = t
