{-# LANGUAGE ViewPatterns #-}

module Arith where

type ParsedTerm = Maybe Term

data Term = TmTrue
    | TmFalse
    | TmIf Term Term Term
    | TmZero
    | TmSucc Term
    | TmPred Term
    | TmIsZero Term
    deriving (Show)

-- should be TmSucc TmZero
term1 = TmSucc TmZero
-- should be TmSucc TmTrue
term2 = TmSucc TmTrue
-- should be TmZero
term3 = TmIf (TmIsZero TmZero) (TmPred (TmSucc TmZero)) TmZero
-- should be TmZero
term4 = TmIf (TmIsZero (TmSucc TmZero)) term1 TmZero
-- should be TmIf TmZero TmZero TmZero
term5 = TmIf TmZero TmZero TmZero


isnumericval TmZero     = True
isnumericval (TmSucc t) = isnumericval t
isnumericval _          = False

isval TmTrue             = True
isval TmFalse            = True
isval t | isnumericval t = True
isval _                  = False

eval1 :: Term -> ParsedTerm
eval1 (TmIf TmTrue  t2 t3)                       = Just t2
eval1 (TmIf TmFalse _  t )                       = Just t
eval1 (TmIf t1      t2 t3)                       = eval1 t1 >>= \t1' -> eval1 (TmIf t1' t2 t3)
eval1 (TmSucc t          )                       = eval1 t >>= Just . TmSucc
eval1 (TmPred TmZero     )                       = Just TmZero
eval1 (TmPred (TmSucc nv1)) | isnumericval nv1   = Just nv1
eval1 (TmPred   t     )                          = eval1 t >>= \t' -> eval1 $ TmPred t'
eval1 (TmIsZero TmZero)                          = Just TmTrue
eval1 (TmIsZero (TmSucc nv1)) | isnumericval nv1 = Just TmFalse
eval1 (TmIsZero t)                               = eval1 t >>= \t' -> eval1 $ TmIsZero t'
eval1 _                                          = Nothing

-- Ex 4.2.1
-- Recursive calls with try handlers might be bad because you always register new handlers https://stackoverflow.com/a/8567429/12035298
-- and in this case it's unneccessary since you always stop after the first time it's triggered
evalSmall :: Term -> Term
evalSmall t =
  let pt = eval1 t
  in  case pt of
        Nothing -> t
        Just t' -> evalSmall t'

-- Ex 4.2.2
-- big-step evaluator
eval2 :: Term -> ParsedTerm
eval2 v | isval v = Just v
-- it's a bit ugly but I tried to match the inference rules.
-- and I think the isval here is even unneccessary since the Just case of eval2 should always be a value
eval2 (TmIf t1 t2 _)
  | Just TmTrue <- eval2 t1
  , Just v <- eval2 t2
  , isval v = Just v
eval2 (TmIf t1 _ t3)
  | Just TmFalse <- eval2 t1
  , Just v <- eval2 t3
  , isval v = Just v
eval2 (TmSucc t)
  | Just nv <- eval2 t
  , isnumericval nv = Just $ TmSucc nv
eval2 (TmPred t)
  | Just TmZero <- eval2 t = Just TmZero
eval2 (TmPred t)
  | Just (TmSucc nv) <- eval2 t
  , isnumericval nv = Just nv
eval2 (TmIsZero t)
  | Just TmZero <- eval2 t = Just TmTrue
eval2 (TmIsZero t)
  | Just (TmSucc nv) <- eval2 t
  , isnumericval nv = Just TmFalse
eval2 _           = Nothing

evalBig :: Term -> Term
evalBig (eval2 -> Just t) = t
evalBig t                 = t



main :: IO ()
main = do
  let x = TmTrue
  putStrLn ""
